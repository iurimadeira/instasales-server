InstasalesServer::Application.routes.draw do  

  # Routes for search and explore
  match 'users/search/', :controller => 'users', :action => 'search', :via => 'get', defaults: {format: :json}
  match 'products/search/', :controller => 'products', :action => 'search', :via => 'get', defaults: {format: :json}
  match 'tags/search/', :controller => 'tags', :action => 'search', :via => 'get', defaults: {format: :json}

  # Routes for followers and following
  match 'followers/', :controller => 'follows', :action => 'followers', :via => 'get', defaults: {format: :json}
  match 'following/', :controller => 'follows', :action => 'following', :via => 'get', defaults: {format: :json}
  match 'follow/', :controller => 'follows', :action => 'follow', :via => 'post', defaults: {format: :json}
  match 'unfollow/', :controller => 'follows', :action => 'unfollow', :via => 'delete', defaults: {format: :json}

  resources :activities, defaults: {format: :json}  

  devise_for :users, :controllers => { :sessions => "sessions" }
  
  devise_scope :user do    
    resources :sessions, :only => [:create, :destroy], defaults: {format: :json}         
  end
  
  resources :users, :only => [:create, :show, :update, :edit, :destroy], defaults: {format: :json}

  resources :products, :except => [:new, :edit, :update], defaults: {format: :json} do    
    resources :comments, :except => [:index, :show], defaults: {format: :json}
    resources :likes, :only => [:create, :destroy, :index], defaults: {format: :json}
  end     

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root :controller => "products", :action => "index", :defaults => {:format => :json}

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
