class User < ActiveRecord::Base
	# Include default devise modules. Others available are:
	# :token_authenticatable, :confirmable,
	# :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable, :registerable, :token_authenticatable,
	       :recoverable, :rememberable, :trackable, :validatable
    
    has_many :products
    has_many :likes

    #Followers system
	has_many :follows, :class_name => "Follow", :foreign_key => "followed_id"
	has_many :following, :through => :follows, :source => :followed
	has_many :inverse_follows, :class_name => "Follow", :foreign_key => "follower_id"
	has_many :followers, :through => :inverse_follows, :source => :follower

	def follow (followed_id)
		Follow.create :follower_id => self.id, :followed_id => followed_id
	end	

	def unfollow (followed_id)
		Follow.where(:follower_id => self.id, :followed_id => followed_id).first.delete
	end

	def follows?(followed_id)
		!Follow.where(:follower_id => self.id, :followed_id => followed_id).blank?
	end	

end
