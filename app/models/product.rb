class Product < ActiveRecord::Base
	include PublicActivity::Model
	tracked owner: ->(controller, model) { controller && controller.current_user }

	acts_as_taggable

	belongs_to :user, :class_name => "User", :foreign_key => "user_id"
	has_many :comments
	has_many :likes

	def extract_tags
		 # Parses hashtags
		 tags = self.description.scan(/#\S+/)

		 # Trim the hashes
		 tags.each_with_index do |tag, index|
		 	tag = tag[1..tag.size]
		 	tags[index] = tag
		 end

		 self.tag_list = tags
	end

end


