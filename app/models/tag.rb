class Tag < ActsAsTaggableOn::Tag

	def self.search_tags(name)
		tag_list = ActsAsTaggableOn::Tag.where("name like ?", "%#{name}%")

		tag_list
	end	

end