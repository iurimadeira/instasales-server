json.extract! @product, :id, :image, :description, :price, :created_at, :updated_at
json.user @product.user, :id, :email
json.comments @product.comments, :id, :author_id, :body
