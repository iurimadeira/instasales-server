json.array!(@products) do |product|
  json.extract! product, :id, :image, :description, :price
  json.url product_url(product, format: :json)
  json.user product.user, :id, :email
  json.comments product.comments, :id, :author_id, :body
end