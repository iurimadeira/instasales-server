json.array!(@users) do |user|
	json.extract! user, :id, :email   
	json.set! :following, current_user.follows?(user.id)
end


