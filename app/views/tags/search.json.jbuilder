json.array!(@tags) do |tag|
	json.extract! tag, :id, :name
	json.set! :taggings, tag.taggings.size	
end
