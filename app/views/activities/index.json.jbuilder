json.array!(@activities) do |activity|
  json.extract! activity, :id, :owner_id, :owner_type, :key, :trackable_id, :trackable_type, :recipient_id, :recipient_type, :created_at
end
