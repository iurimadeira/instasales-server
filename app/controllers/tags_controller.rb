class TagsController < ApplicationController
  def search
    @tags = Tag.search_tags(params[:name])

    # pagination
    if params[:page].to_i <= Kaminari.paginate_array(@tags).page(1).per(10).num_pages
      @tags = Kaminari.paginate_array(@tags).page(params[:page]).per(10)
      return @tags
    end

    @tags = nil
    
  end
end
