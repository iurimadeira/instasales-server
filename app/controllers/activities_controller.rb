class ActivitiesController < ApplicationController
  def index
  	# Add comments on your products
  	comments_on_my_products = PublicActivity::Activity.where(trackable_type: "Comment", recipient_id: current_user.products, recipient_type: "Product")

  	# Add products from your following
  	products_from_my_following = PublicActivity::Activity.where(trackable_type: "Product", owner_id: current_user.following, owner_type: "User")

  	# Add followings to you  	
  	people_following_me = PublicActivity::Activity.where(trackable_type: "Follow", recipient_type: "User", recipient_id: current_user.id )

  	# Merge relations
  	@activities = comments_on_my_products | products_from_my_following | people_following_me

  	# Sort by date
  	@activities.sort do |a, b|
        a.created_at <=> b.created_at
    end

    # Desc
    @activities.reverse

    # pagination
    if params[:page].to_i <= Kaminari.paginate_array(@activities).page(1).per(10).num_pages
      @activities = Kaminari.paginate_array(@activities).page(params[:page]).per(10)
      return @activities
    end

    @activities = nil

  end
end

