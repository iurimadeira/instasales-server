class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception
  respond_to :json  

  def current_user
    super
  end

  helper_method :current_user
  hide_action :current_user

end
