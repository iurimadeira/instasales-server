class SessionsController < Devise::SessionsController  
   protect_from_forgery :except => [:create, :destroy]

   def create
      resource = warden.authenticate!(scope: resource_name, recall: "#{controller_path}#new")
      set_flash_message(:notice, :signed_in) if is_navigational_format?
      sign_in(resource_name, resource)
      current_user.ensure_authentication_token
      current_user.save      

      respond_to do |format|
        format.html do
          render json: { response: 'ok', auth_token: current_user.authentication_token }.to_json, status: :ok
        end
        format.json do
          render json: { response: 'ok', auth_token: current_user.authentication_token }.to_json, status: :ok
        end
      end

    end

    def destroy
      respond_to do |format|
        format.html { 
          warden.authenticate!(:scope => resource_name, :recall => "#{controller_path}#new")
          current_user.authentication_token = nil
          current_user.save
          render :json => {}.to_json, :status => :ok
        }
        format.json {
          warden.authenticate!(:scope => resource_name, :recall => "#{controller_path}#new")
          current_user.authentication_token = nil
          current_user.save
          render :json => {}.to_json, :status => :ok
        }
      end
    end

end  