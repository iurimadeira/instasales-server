class FollowsController < ApplicationController
  before_action :set_follow, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user! 

  # GET /follows
  # GET /follows.json
  def followers
    @follows = current_user.followers

    # pagination
    if params[:page].to_i <= Kaminari.paginate_array(@follows).page(1).per(10).num_pages
      @follows = Kaminari.paginate_array(@follows).page(params[:page]).per(10)
      return @follows
    end

    @likes = nil
    
  end

  def following
    @follows = current_user.following

    # pagination
    if params[:page].to_i <= Kaminari.paginate_array(@follows).page(1).per(10).num_pages
      @follows = Kaminari.paginate_array(@follows).page(params[:page]).per(10)
      return @follows
    end

    @likes = nil
    
  end

  # POST /follows
  # POST /follows.json
  def follow

    respond_to do |format|
      if (current_user.follow params[:followed_id])                
        format.json { render json: { ok: true } }
      else        
        format.json { render json: { error: "Failed to follow user." } }
      end      
      
    end

  end

  # DELETE /follows/1
  # DELETE /follows/1.json
  def unfollow   

    respond_to do |format|
      if (current_user.unfollow params[:followed_id])                
        format.json { render json: { ok: true } }
      else        
        format.json { render json: { error: "Failed to unfollow user." } }
      end            
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_follow
      @follow = follow.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def follow_params
      params.require(:follow).permit(:follower_id, :followed_id)
    end
end
