class RenameUserIdToFollowerIdAndFriendIdToFollowedId < ActiveRecord::Migration
  def change
  	add_column :follows, :follower_id, :integer
  	remove_column :follows, :user_id

  	add_column :follows, :followed_id, :integer
  	remove_column :follows, :friend_id
  end
end
