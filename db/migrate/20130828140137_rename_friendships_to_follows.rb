class RenameFriendshipsToFollows < ActiveRecord::Migration
  def change
  	rename_table :friendships, :follows
  end
end
