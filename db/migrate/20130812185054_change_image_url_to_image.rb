class ChangeImageUrlToImage < ActiveRecord::Migration
  def change
  	add_column :products, :image, :text
  	remove_column :products, :image_url
  end
end
